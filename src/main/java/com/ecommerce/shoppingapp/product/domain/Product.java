package com.ecommerce.shoppingapp.product.domain;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Indexed;

import java.math.BigDecimal;
import java.util.List;

@Document(collation = "product")
@Getter
@Setter
@Builder
@EqualsAndHashCode(of = "id")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Product {

    String id;
    String name;
    String code;
    String description;
    String companyId;
    String features;
    BigDecimal price;
    String categoryId;
    List<ProductImage> productImages;
    Boolean active;






}
