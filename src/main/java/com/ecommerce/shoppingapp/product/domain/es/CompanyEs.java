package com.ecommerce.shoppingapp.product.domain.es;

import lombok.Data;

@Data
public class CompanyEs {
    String id;
    String name;
    String code;

}
