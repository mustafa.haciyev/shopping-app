package com.ecommerce.shoppingapp.product.domain.es;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.elasticsearch.annotations.Document;

import java.math.BigDecimal;

@Document(indexName = "product")
@Getter
@Setter
@EqualsAndHashCode(of = "id")
public class ProductEs {

    String id;
    String name;
    String code;
    String description;
    CompanyEs seller;
    String features;
    BigDecimal price;
    CategoryEs category;
    Boolean active;


}
