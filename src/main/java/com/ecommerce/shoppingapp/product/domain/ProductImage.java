package com.ecommerce.shoppingapp.product.domain;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)

public class ProductImage {
    ImageType imageType;
    String url;


   public enum ImageType{
        FEATURE, NORMAL;

    }
}
