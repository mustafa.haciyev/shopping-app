package com.ecommerce.shoppingapp.product.domain;

public enum MoneyTypes {
    USD("Dollar", "$"),
    EUR("Euro","E"),
    TL("Turk lirasi","TL");

    private String label;
    private String symbol;

    MoneyTypes(String label, String symbol) {
        this.label = label;
        this.symbol = symbol;
    }
}
