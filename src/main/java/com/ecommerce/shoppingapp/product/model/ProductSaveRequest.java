package com.ecommerce.shoppingapp.product.model;

import com.ecommerce.shoppingapp.product.domain.MoneyTypes;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;
import java.util.List;

@Builder
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ProductSaveRequest {

    String id;
    String name;
    String description;
    String features;
    BigDecimal available;
    BigDecimal price;
    MoneyTypes money;
    List<String> images;
    String sellerId;
    String categoryId;




}
