package com.ecommerce.shoppingapp.product.model;

import com.ecommerce.shoppingapp.product.domain.MoneyTypes;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;

@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ProductResponse {

    String id;
    String image;
    String name;
    String description;
    ProductSellerResponse seller;
    String features;
    int available;
    boolean freeDelivery;
    String deliveryIn;
    BigDecimal price;
    String categoryId;
    MoneyTypes moneyType;


}
